% Source: http://math.stackexchange.com/questions/280937/finding-the-angle-of-rotation-of-an-ellipse-from-its-general-equation-and-the-ot
% Ellipse coefficients:
%   Ax1^2 + Bx1x2 + Cx2^2 + Dx1 + Ex2 + F = 0

function [ thetas, x1s, x2s ] = roa_points( S, rho, xG )
    % Define existing ellipse coefficients
    A = S(1,1);
    B = S(1,2)+S(2,1);
    C = S(2,2);
    
    % Compute angle of rotation
    alpha = 0.5*atan(B/(A-C));
    
    % Define rotated coefficients
    rotA = A*cos(alpha)^2+B*cos(alpha)*sin(alpha)+C*sin(alpha)^2;
    rotB = 0.0;
    rotC = A*sin(alpha)^2-B*cos(alpha)*sin(alpha)+C*cos(alpha)^2;
    rotF = -rho;
    
    % Find major and minor axes of rotated ellipse
    a = sqrt((-4*rotF*rotA*rotC)/(4*rotA*rotC^2));
    b = sqrt((-4*rotF*rotA*rotC)/(4*rotA^2*rotC));
    
    % Define rotation matrix
    R = [cos(alpha-pi/2), -sin(alpha-pi/2);
         sin(alpha-pi/2), cos(alpha-pi/2) ];
    
    % Create storage vectors for theta, x1, and x2
    thetas = -pi:0.25:pi;
    x1s = [];
    x2s = [];
    
    % Iterate through points on ellipse
    for theta=thetas
        % Compute point on unrotated ellipse
        x = [a*cos(theta);
             b*sin(theta)];
        
        % Rotate and translate to actual position
        x = R*x+xG;
        
        % Update x1 to valid position
        % if abs(x(1))>pi
        %     x(1) = x(1)-sign(x(1))*2*pi;
        % end
        
        % Add values to storage vectors
        x1s = [x1s x(1)];
        x2s = [x2s x(2)];
    end
    
    x1s = [x1s x1s(1)];
    x2s = [x2s x2s(1)];
end
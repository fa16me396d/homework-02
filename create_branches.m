function [center_set,path_set,cost_set] = create_branches(num_branches)
% Creates sets of ROA centers, paths, and costs for a given number of
%       branches, which should be 13 according to Tedrake
% Does NOT plot the ROAs, trajectories and tubes

center_set =[];
cost_set = [];
Nm = 2;
err = 1e-3;

    % for each center of the ROA for each branch
    for c = 1:num_branches
        if c == 1
            % setting up ROA for final goal point
            xG = [pi;0.0];
            center_set(:,c) = xG;
            cost_set(c) = 0;
            path_set{c} = xG;
        else
            [x_new,new_cost,new_path,xG_min] = find_least_cost_point(center_set,cost_set,path_set);
            center_set(:,c) = x_new;
            cost_set(c) = new_cost;
            path_set{c} = new_path;
        end     
    end
end
function [ retVal ] = fcl( xbar, xG, K )
    % Gravitational constant
    g = 9.8;
    % Pendulum mass
    m = 1.0;
    % Pendulum length
    l = 0.5;
    % Friction coefficient
    b = 0.1;
    
    % Compute return value
    f2x1 = -g/l*cos(xG(1))-K(1)/(m*l^2);
    f2x2 = -b/(m*l^2)-K(2)/(m*l^2);
    f2x1_2 = g/l*sin(xG(1));
    f2x1_3 = g/l*cos(xG(1));
    retVal = [xbar(2)               ;
              xbar(1)*f2x1+xbar(2)*f2x2 + ...
                  xbar(1)^2*f2x1_2/2 + ...
                  xbar(1)^3*f2x1_3/6];
end
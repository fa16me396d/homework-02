function [ A, B, K, S, E ] = lti_system( xG )
    % Gravitational constant
    g = 9.8;
    % Pendulum mass
    m = 1.0;
    % Pendulum length
    l = 0.5;
    % Friction coefficient
    b = 0.1;
    
    % State equation A matrix
    A = [0.0            , 1.0       ;
         -g/l*cos(xG(1)), -b/(m*l^2)];
    % State equation B matrix
    B = [0.0        ;
         1.0/(m*l^2)];
     
    % State cost matrix
    Q = diag([10.0, 1.0]);
    % Control cost matrix
    R = 20.0;
    % Cross cost matrix
    N = [0.0;
         0.0];
    
    % Compute LQR parameters
    [K,S,E] = lqr(A, B, Q, R, N);
end
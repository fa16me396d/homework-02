function [x_new,new_cost,new_path,xG_min] = find_least_cost_point(center_set,cost_set,path_set)
    % center_set --- a 2xn matrix for n number of existing ROA centers
    % cost_set --- a 1xn matrix for path cost of each center to final goal
    % path_set --- a cell of length n that stores the backward path of 
    %       points as 2xm matrices from each center to the final goal
    
    Nm = 2;
    err = 1e-3;
    
    % if random points used to generate new initial points
    x_in_ROAs = true;
    % check that random point is not in one of the existing ROAs
    while x_in_ROAs == true
        x1 = rand*2*pi-pi/2;
        x2 = rand*20-10;
        % assume random point is outside ROAs until proved otherwise
        x_in_ROAs = false;
        
        for w = 1:length(cost_set)
            xG_temp = [center_set(1,w);center_set(2,w)];
            % Obtain system matrices
            [A, B, K, S, E] = lti_system(xG_temp);
            [rho] = find_rho_TV(xG_temp, S, K, Nm, err, A, B);
            % If new initial point is in the existing ROAs, then set
            %       x_in_ROAs to be true and choose another initial point
            % General Equation for ellipse:
            % A*x1^2 + B*x1*x2 + C*x2^2 + F = 0
            %       with
            %           A = S(1,1)
            %           B = S(1,2)+S(2,1)
            %           C = S(2,2)
            %           F = -rho
            if (S(1,1)*x1^2 + (S(1,2)+S(2,1))*x1*x2 + S(2,2)*x2^2)<=rho
                x_in_ROAs = true;
            end            
        end
    end
    
    x_new = [x1; x2];
    % With new initial point (x1,x2), find the existing center with least
    %       cost
    for p = 1:length(cost_set)
        xG = [center_set(1,p);center_set(2,p)];
        [Xsoln, cost] = PendulumTrajectory(x_new, xG);
        %path_cost = 0;
        path_cost = cost_set(p);
        [m,n] = size(path_set{p});
        temp_cost = path_cost + cost;
        if p == 1
            new_cost = temp_cost;
            xG_min = xG;
            new_path = path_set{p};
            new_path(:,n+1) = x_new;
        elseif temp_cost < new_cost
            new_cost = temp_cost;
            xG_min = xG;
            new_path = path_set{p};
            new_path(:,n+1) = x_new;
        end
    end       
end
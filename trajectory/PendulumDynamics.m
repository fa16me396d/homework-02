function dZ = PedulumDynamics(~, Z, u, P)

%% Define constants
% Gravitational constant
g = P.dyn.gravity;

%% Define system parameters
% Pendulum mass
m = P.dyn.mass;
% Pendulum length
l = P.dyn.length;
% Friction coefficient
b = P.dyn.coeffRestitution;

%% Define system dynamics
theta = Z(1,:);
omega = Z(2,:);

dtheta = omega;
domega = -g/l*sin(theta) - b/(m*l^2)*omega + u/(m*l^2);

%% Define cost function
R = P.cost.Matrix.R;
dcost = R * u.^2;

%% Pack up
dZ = [dtheta; domega; dcost];

end




%% Clear all variables
clear all;

%% Define constants
% Gravitational constant
g = 9.8;

%% Define system parameters
% Pendulum mass
m = 1.0;
% Pendulum length
l = 0.5;
% Friction coefficient
b = 0.1;

%% Define system equation
f = @(x) [x(2);
          -g/l*sin(x(1))-b/(m*l^2)*x(2)];

%% Setup simulation
% Define time horizon
T = 60.0;
% Define time step
dt = 0.01;

% Create and initialize state vector
x = [deg2rad(45);
     0.0];
% Create and initialize results matrix
xs = x;

%% Run simulation
for i=dt:dt:T
    % Compute xdot
    xdot = f(x);
    
    % Update x
    x = x + xdot*dt;
    
    % Append result to results matrix
    xs = [xs x];
end

%% Plot results
figure(1);
plot(0:dt:T, rad2deg(xs(1, :)));
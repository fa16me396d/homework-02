%% Clear all variables
clear all;

%% Disable warnings
warning('off', 'all');

%% Define goal points
% Goal state
xG = [deg2rad(180.0);
      0.0           ];

%% Define LTI closed-loop system
% Obtain system matrices
[A, B, K, S, E] = lti_system(xG);

%% Compute rho
% Compute rho
rho = find_rho(xG, S, K, 2, 1e-3);

%% Plot results
% Plot region of attraction
figure(1);
scatter([xG(1)], [xG(2)], [5], 'b', 'filled');
hold on;
[thetas, x1s, x2s] = roa_points(S, rho, xG);
plot_ellipse(x1s, x2s, 'k');
axis([-0.5*pi 1.5*pi -10.0 10.0]);
function [  ] = plot_ellipse( x1s, x2s, color )
    % Identify points that are okay
    idx_ok = and(x1s>=-0.5*pi, x1s<=1.5*pi);
    x1s_ok = x1s(idx_ok);
    x2s_ok = x2s(idx_ok);
    
    % Identify points that wrap right
    idx_r = x1s>1.5*pi;
    x1s_r = x1s(idx_r)-2*pi;
    x2s_r = x2s(idx_r);
    
    % Identify points that wrap left
    idx_l = x1s<-0.5*pi;
    x1s_l = x1s(idx_l)+2*pi;
    x2s_l = x2s(idx_l);
    
    % Plot ellipse
    plot(x1s_ok, x2s_ok, color, ...
         x1s_r, x2s_r, color, ...
         x1s_l, x2s_l, color);
end
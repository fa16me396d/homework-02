%% Clear all variables
clear all;

xSoln = PendulumTrajectory([0.0; 0.0], [pi; 0.0]);
clf
Time = linspace(0,xSoln.duration,20);
rho_prev = 0;
for step = 20:-1:1
%% Disable warnings
warning('off', 'all');

%% Define goal points
if step == 20
    % Goal state
    xG = [deg2rad(180.0);
          0.0           ];
else
    % angle at time t as solved by the trajectory code
    xG = [xSoln.state(1,step);
          xSoln.state(2, step)            ];
end

%% Define LTI closed-loop system
% Obtain system matrices
[A, B, K, S, E] = lti_system(xG);

%% Compute rho
% Compute rho
rho = find_rho_TV(xG, S, K, 2, 1e-3,A,B);
dt = xSoln.duration/20;
% rho = find_rho_TV_2(xG, S, K, 2, 1e-3,A,B,step,rho_prev,dt);
rho_prev = 0;
fprintf('t = %d    rho = %d    x(t) = %d\n',Time(step),rho,xG(1))

%% Plot results
% Plot region of attraction
figure(1);
scatter([xG(1)], [xG(2)], [5], 'b', 'filled');
hold on;
[thetas, x1s, x2s] = roa_points(S, rho, xG);
plot_ellipse(x1s, x2s, 'k');
axis([-0.5*pi 1.5*pi -10.0 10.0]);

end

figure(1)
plot(xSoln.state(1,:),xSoln.state(2,:),'k-');
plot(xSoln.state(1,:),xSoln.state(2,:),'ko','MarkerSize',10)
xlabel('Angle (rad)')
ylabel('Rate (rad/s)')

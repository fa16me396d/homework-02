function [ rho ] = find_rho_TV( xG, S, K, Nm, err, A, B, step, rho_prev)
    % Create symbolic xbar elements
    pvar xbar1 xbar2;
    xbar = [xbar1; xbar2];
    
    % Initialize binary search
    rho_lo = 0.0;
    rho_hi = 100.0;
    
    % Define system polynomials
    Q = diag([10,1]);
    R = 20;
    % Compute rho
    if step == 20
        S = Q;
    end

    J = xbar'*S*xbar;
    Sdot = -(Q-S*B*inv(R)*B'*S + S*A + A'*S);
    Jdot = xbar'*Sdot*xbar + 2*xbar'*S*fcl(xbar, xG, K);
    %Jdot = 2*xbar'*S*fcl(xbar, xG, K);
    hm = monomials(xbar, [0:Nm]);
    h = hm'*mpvar('d', [length(hm), 1]);
    h2 = hm'*mpvar('d', [length(hm), 1]);
    h3 = hm'*mpvar('d', [length(hm), 1]);
    
    % Binary line search to find rho
    while (rho_hi-rho_lo)>0.001
        % Compute midpoint
        rho_mid = mean([rho_hi, rho_lo]);
        if step == 20
            rho_dot = 0;
        else
            rho_dot = (rho_prev-rho_mid)/2;
        end
        % Create proble constraints
        pconstr = [Jdot-rho_dot+h*(rho_mid-J)+h2+h3 <0;
                   h >= 0; h2>=0; h3>=0];
               
        % Obtain SOSOpt options
        opts = sosoptions;

        % Try to solve SOS problem
        [info, dopt, sossol] = sosopt(pconstr, xbar, opts);

        % Update rho bounds based on problem feasibility
        if info.feas
            rho_lo = rho_mid;
        else
            rho_hi = rho_mid;
        end
    end
    
    rho = rho_lo;
end
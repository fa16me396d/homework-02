function [  ] = plot_traj( xis, clr_xis )
    % Compute the number of points
    n = length(xis(1, :));
    eta = 1.0/n;
    
    % Create storage vectors
    xi1s = [];
    xi2s = [];
    
    for i=1:n-1
        x1 = xis(:, i);
        x2 = xis(:, i+1);
        
        for j=0:eta:1.0-eta
            x = x1+j*(x2-x1);
            
            xi1s = [xi1s, x(1)];
            xi2s = [xi2s, x(2)];
        end
    end
    
    xi1s = [xi1s, xis(1, end)];
    xi2s = [xi2s, xis(2, end)];
    
    % Identify points that are okay
    idx_ok = and(xi1s>=-0.5*pi, xi1s<=1.5*pi);
    xi1s_ok = xi1s(idx_ok);
    xi2s_ok = xi2s(idx_ok);
    
    % Identify points that wrap right
    idx_r = xi1s>1.5*pi;
    xi1s_r = xi1s(idx_r)-2*pi;
    xi2s_r = xi2s(idx_r);
    
    % Identify points that wrap left
    idx_l = xi1s<=-0.5*pi;
    xi1s_l = xi1s(idx_l)+2*pi;
    xi2s_l = xi2s(idx_l);
    
    % Plot the trajectory
    plot(xi1s_ok, xi2s_ok, clr_xis, ...
            xi1s_r, xi2s_r, clr_xis, ...
            xi1s_l, xi2s_l, clr_xis);
    drawnow;
end
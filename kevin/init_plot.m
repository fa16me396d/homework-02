function [  ] = init_plot(  )
    % Create Figure 1 and set view
    figure(1);
    hold on;
    axis([-0.5*pi, 1.5*pi, -10.0, 10.0]);
    
    % Create plot title and axis labels
    title('$$ \dot{\theta}\ vs.\ \theta $$', ...
              'interpreter', 'latex', ...
              'fontsize', 16);
    xlabel('$$ \theta $$', ...
               'interpreter', 'latex', ...
               'fontsize', 12);
    ylabel('$$ \dot{\theta} $$', ...
               'interpreter', 'latex', ...
               'fontsize', 12);

    % Show the Figure immediately
    drawnow;
end
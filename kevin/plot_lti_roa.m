function [  ] = plot_lti_roa( P, xi )
    %% Create LQR controller for RoA around xi
    % Linearize the system about xG
    [A, B] = lin_fcn(P, xi);
    % Obtain the LQR matrices
    [K, S, E] = lqr(A, B, P.Q, P.R, P.N);
    % Compute rho
    rho = find_rho(P, xi, S, K);

    %% Find and draw the RoA around the xi
    % Find points on the ellipse
    [thetas, xs] = ellipse_points(S, rho, xi);
    % Plot the RoA
    plot_roa(xi, xs, 'b', 'k');
end
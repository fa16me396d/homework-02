function [ rho ] = find_rho( P, xi, S, K )
    % Create symbolic xbar elements
    pvar xbar1 xbar2;
    xbar = [xbar1; xbar2];
    
    % Initialize binary search bounds
    rho_lo = 0.0;
    rho_hi = 100.0;
    
    % Define system polynomials
    J = xbar'*S*xbar;
    Jdot = 2*xbar'*S*fcn_cl(P, xbar, xi, K);
    
    % Define lagrangian polynomial
    hm = monomials(xbar, [0:P.Nm]);
    h = hm'*mpvar('d', [length(hm), 1]);
    
    % Binary line search to find rho
    while (rho_hi-rho_lo)>0.01
        % Compute midpoint
        rho_mid = mean([rho_hi, rho_lo]);
        
        % Create problem constraints
        pconstr = [Jdot+h*(rho_mid-J) <= -P.err*xbar'*xbar;
                   h >= 0                               ];
        
        % Obtain SOSOpt options
        opts = sosoptions;
        
        % Try to solve SOS problem
        [info, dopt, sossol] = sosopt(pconstr, xbar, opts);
        
        % Update search bounds
        if info.feas
            rho_lo = rho_mid;
        else
            rho_hi = rho_mid;
        end
    end
    
    % Return greatest feasible rho
    rho = rho_lo;
end
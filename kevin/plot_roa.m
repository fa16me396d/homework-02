function [  ] = plot_roa( xi, xs, clr_xi, clr_xs )
    % Extract x1s and x2s from xs
    x1s = xs(1, :);
    x2s = xs(2, :);

    % Identify points that are okay
    idx_ok = and(x1s>=-0.5*pi, x1s<=1.5*pi);
    x1s_ok = x1s(idx_ok);
    x2s_ok = x2s(idx_ok);
    
    % Identify points that wrap right
    idx_r = x1s>=1.5*pi;
    x1s_r = x1s(idx_r)-2*pi;
    x2s_r = x2s(idx_r);
    
    % Identify points that wrap left
    idx_l = x1s<=-0.5*pi;
    x1s_l = x1s(idx_l)+2*pi;
    x2s_l = x2s(idx_l);
    
    % Plot xi
    scatter([xi(1)], [xi(2)], [10], clr_xi, 'filled');
    
    % Plot ellipse
    plot(x1s_ok, x2s_ok, clr_xs, ...
             x1s_r, x2s_r, clr_xs, ...
             x1s_l, x2s_l, clr_xs);

    % Draw the ellipse right away
    drawnow;
end
function [ rho ] = find_rho( P, xi, S, Sdot, K, rho_prev, dt )
    % Create symbolic xbar elements
    pvar xbar1 xbar2;
    xbar = [xbar1; xbar2];
    
    % Define system polynomials
    J = xbar'*S*xbar;
    Jdot = xbar'*Sdot*xbar + 2*xbar'*S*fcn_cl(P, xbar, xi, K);
    
    % Define lagrangian polynomial
    hm = monomials(xbar, [0:P.Nm]);
    h1 = hm'*mpvar('a', [length(hm), 1]);
    h2 = hm'*mpvar('b', [length(hm), 1]);
    
    % Initialize rho
    rho = rho_prev;
    
    % Run the SOSOpt code with this rho
    pconstr = [Jdot+h1*(rho-J)+h2*dt <= -P.err*xbar'*xbar;
               h2 >= 0];
    opts = sosoptions;
    [info, dopt, sossol] = sosopt(pconstr, xbar, opts);
    
    % If the existing rho is no good, binary search
    if ~info.feas
        % Initialize binary search bounds
        rho_lo = 0.0;
        rho_hi = 100.0;
        
        while (rho_hi-rho_lo)>0.01
            % Compute midpoint
            rho_mid = mean([rho_hi, rho_lo]);

            % Create problem constraints
            pconstr = [Jdot+h1*(rho_mid-J)+h2*dt <= -P.err*xbar'*xbar;
               h2 >= 0];
            opts = sosoptions;
            [info, dopt, sossol] = sosopt(pconstr, xbar, opts);

            % Update search bounds
            if info.feas
                rho_lo = rho_mid;
            else
                rho_hi = rho_mid;
            end
        end
        
        rho = rho_lo;
    end
end
function [  ] = plot_tunnel( P, xis )
    % Compute initial S and rho and plot RoA
    xi = xis(:, end);
    [A, B] = lin_fcn(P, xi);
    [K, S, E] = lqr(A, B, P.Q, P.R, P.N);
    rho = find_rho(P, xi, S, K);
    [thetas, xs] = ellipse_points(S, rho, xi);
    plot_roa(xi, xs, 'b', 'k');
    
    % Compute dt
    dt = 1.5/99.0;
    
    for i=length(xis(1,:)):-1:2
        % Extract xi
        xi = xis(:, i);
        
        % Linearize the system
        [A, B] = lin_fcn(P, xi);
        % Compute Sdot
        Sdot = -P.Q + S*B*inv(P.R)*B'*S - S*A - A'*S;
        
        % RK4
        K1 = Sdot;
        [A, B] = lin_fcn(P, (xi+xis(:, i-1))/2.0);
        K2 = -P.Q + (S+dt/2.0*K1)*B*inv(P.R)*B'*(S+dt/2.0*K1) - ...
            (S+dt/2.0*K1)*A - A'*(S+dt/2.0*K1);
        K3 = -P.Q + (S+dt/2.0*K2)*B*inv(P.R)*B'*(S+dt/2.0*K2) - ...
            (S+dt/2.0*K2)*A - A'*(S+dt/2.0*K2);
        [A, B] = lin_fcn(P, xis(:, i-1));
        K4 = -P.Q + (S+dt*K3)*B*inv(P.R)*B'*(S+dt*K3) - ...
            (S+dt*K3)*A - A'*(S+dt*K3);
        
        % Step backwards to previous S
        % S = S - dt/6.0*(K1+2*K2+2*K3+K4);
        S = S - dt*Sdot;
        % Update rho
        K = inv(P.R)*B'*S;
        rho = find_rho_ltv(P, xi, S, Sdot, K, rho, dt);
        
        % Extract xi for plotting
        xi = xis(:, i-1);
        
        % Move xi back in bounds
        if xi(1) > 1.5*pi
            xi(1) = xi(1) - 2*pi;
        end
        if xi(1) <= -0.5*pi
            xi(1) = xi(1) + 2*pi;
        end
        
        % Find ellipse points and draw
        [thetas, xs] = ellipse_points(S, rho, xi);
        plot_roa(xi, xs, 'b', 'k');
    end
end
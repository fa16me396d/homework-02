function [ A, B ] = lin_fcn( P, xi )
    %% Linearize system about xi
    % Compute A matrix for linearized system
    A = [0.0                , 1.0     ;
         -P.g/P.l*cos(xi(1)), -P.b/P.I];
    % Compute B matrix for linearized system
    B = [0.0    ;
         1.0/P.I];
end
function [ soln ] = phys_int( P, x )
% Create persistent variables
persistent x_last;
persistent soln_last;
persistent callnum;

% Initialize callnum
if isempty(callnum)
    callnum = 0;
end

% Check to see if the solution has already been found
if isequal(x, x_last)
    soln = soln_last;
else
    x_last = x;
    x = convert_state(
end
function [ xi0 ] = next_point(  )
    % Randomly select an initial point
    xi0 = [-0.5*pi+2*pi*rand(1,1);
           -10.0+20.0*rand(1,1)  ];

    % Draw the point
    scatter([xi0(1)], [xi0(2)], [10], 'r', 'filled');
    drawnow;
    
    % Initialize user input to fail
    usr_in = 'n';
    
    while ('n'==usr_in)
        % Ask the user if the point is acceptable
        usr_in = input('Is the point shown in red acceptable? ', 's');
        
        if('n'==usr_in)
            scatter([xi0(1)], [xi0(2)], [10], 'w', 'filled');
            xi0 = [-0.5*pi+2*pi*rand(1,1);
                   -10.0+20.0*rand(1,1)  ];
            scatter([xi0(1)], [xi0(2)], [10], 'r', 'filled');
            drawnow;
        else if ('c'==usr_in)
                scatter([xi0(1)], [xi0(2)], [10], 'w', 'filled');
                drawnow;
                [xi01, xi02] = ginput(1);
                xi0 = [xi01; xi02];
            end
        end
    end
end
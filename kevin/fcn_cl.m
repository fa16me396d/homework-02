function [ xbardot ] = fcn_cl( P, xbar, xi, K )
    %% Compute Taylor expansion components
    f2x1 = -P.g/P.l*cos(xi(1))-K(1)/P.I;
    f2x2 = -P.b/P.I-K(2)/P.I;
    f2x1_2 = P.g/P.l*sin(xi(1));
    f2x1_3 = P.g/P.l*cos(xi(1));
    
    %% Return Taylor approximation result
    xbardot = [xbar(2)                   ;
               xbar(1)*f2x1+xbar(2)*f2x2 + ...
                   xbar(1)^2.0*f2x1_2/2.0 + ...
                   xbar(1)^3.0*f2x1_3/6.0];
end
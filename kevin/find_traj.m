function [ best_ts, best_soln, best_cost ] = find_traj( xi0, xif )
    % Create initial variable to store solution and cost
    [best_ts, best_soln, best_cost] = PendulumTrajectory(xi0, xif);
    
    % Try shooting one period to the right if xi0 is to the right
    if (xi0(1) >= xif(1))
        xif(1) = xif(1) + 2.0*pi;
        [ts, soln, cost] = PendulumTrajectory(xi0, xif);
        % Update best solution and cost
        if (cost<best_cost)
            best_ts = ts;
            best_soln = soln;
            best_cost = cost;
        end
    % Try shooting one period to the left if xi0 is to the left
    else if (xi0(1) <= xif(1))
        xif(1) = xif(1) - 2.0*pi;
        [ts, soln, cost] = PendulumTrajectory(xi0, xif);
        % Update best solution and cost
        if (cost<best_cost)
            best_ts = ts;
            best_soln = soln;
            best_cost = cost;
        end
        end
    end
end
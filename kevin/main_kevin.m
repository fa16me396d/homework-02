%--------------------------------------------------------------------------
%   File:   main_kevin.m
%   Author: Kevin Chen
%--------------------------------------------------------------------------
%   Description
%       This script is the main script used to produce our attempt at
%   replicating the results of "LQR-Trees: Feedback Motion Planning via
%   Sum-of-Squares Verification" by Tedrake, Manchester, Tobenkin, and
%   Roberts. The objective of this project is to show that a piecewise
%   series of time-varying and time-invariant LQR controllers can solve 
%   the inverted simple damped pendulum control problem.
%--------------------------------------------------------------------------

%% Clear everything and disable warnings
clear all; clf;
warning('off', 'all');

%% Initialize plot
init_plot(  );

%% Establish system parameters
P = get_params(  );

%% Find and draw the RoA around the goal
plot_lti_roa(P, P.xG);

%% Initialize branch node recording
xs = [P.xG];
costs = [0.0];

%% Create all of the branches we want
for i=1:P.branches
    %% Poll user for a suitable next point
    xi0 = next_point(  );
    xs = [xs, xi0];
    
    %% Find and draw the RoA around the point
    plot_lti_roa(P, xi0);
    
    %% Find best point to shoot to
    % Create variable to find best solution, init as direct shot to xG
    [best_soln, best_cost] = PendulumTrajectory(xi0, P.xG);
    
    for j=2:length(costs)
        % Find the cost to go to a different point
        [soln, cost] = PendulumTrajectory(xi0, xs(:, j));
        
        if (cost+costs(j) <= best_cost)
            best_soln = soln;
            best_cost = cost+costs(j);
        end
    end
    
    % Update cost storage vector
    costs = [costs, best_cost];
    
    % Draw stuff
    plot_traj(best_soln.state, 'b-.');
    plot_tunnel(P, best_soln.state);
    drawnow;
end
function [ P ] = get_params(  )
    %% Create system parameters
    % Pendulum mass
    P.m = 1.0;
    % Pendulum length
    P.l = 0.5;
    % Pendulum damping coefficient
    P.b = 0.1;
    % Pendulum rotational inertia
    P.I = P.m*P.l^2.0;
    % Target point
    P.xG = [pi; 0.0];
    
    %% Define cost function parameters
    % State variable weights
    P.Q = diag([10.0, 1.0]);
    % Input weight
    P.R = 20.0;
    % Input-state cross weight
    P.N = [0.0; 0.0];
    
    %% Define LQR tree parameters
    % Allowable error for rho SOS computation
    P.err = 1e-3;
    % Monomial degree for SOS computation
    P.Nm = 2;
    % Number of branches to create
    P.branches = 13;
    
    %% Define other constants
    % Gravitational constant
    P.g = 9.8;
end
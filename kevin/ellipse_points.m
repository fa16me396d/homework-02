function [ thetas, xs ] = ellipse_points( S, rho, xi )
    % Define existing ellipse coefficients
    A = S(1, 1);
    B = S(1, 2)+S(2, 1);
    C = S(2, 2);
    
    % Compute ccw rotation
    alpha = 0.5*atan(B/(A-C));
    
    % Define unrotated, centered coefficients
    A_ur = A*cos(alpha)^2.0+B*cos(alpha)*sin(alpha)+C*sin(alpha)^2.0;
    B_ur = 0.0;
    C_ur = A*sin(alpha)^2.0-B*cos(alpha)*sin(alpha)+C*cos(alpha)^2.0;
    F_ur = -rho;
    
    % Find major and minor axes of unrotated ellipse
    a = sqrt(-F_ur/C_ur);
    b = sqrt(-F_ur/A_ur);
    
    % Redefine alpha to be cw rotation
    alpha = alpha-pi/2.0;
    
    % Define rotation matrix
    R = [cos(alpha), -sin(alpha);
         sin(alpha), cos(alpha) ];
    
    % Define internal angles to find points on ellipse
    thetas = -pi:0.01:pi;
    
    % Find unrotated points on ellipse
    xs = [a*cos(thetas);
          b*sin(thetas)];
    
    % Rotate and translate ellipse
    xs = R*xs + xi;
    
    % Add initial point to end of xs and return
    xs = [xs xs(:, 1)];
end
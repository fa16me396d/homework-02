%% Clear all variables
clear all;

%% Define constants
% Gravitational constant
g = 9.8;

%% Define system parameters
% Pendulum mass
m = 1.0;
% Pendulum length
l = 0.5;
% Friction coefficient
b = 0.1;

%% Define goal points
% Goal state and input
xG = [deg2rad(180.0);
      0.0           ];
uG = 0.0;

%% Compute state matrices
% State equation A matrix
A = [0.0            , 1.0       ;
     -g/l*cos(xG(1)), -b/(m*l^2)];
% State equation B matrix
B = [0.0        ;
     1.0/(m*l^2)];

%% Define cost function parameters
% State cost matrix
Q = diag([10.0, 1.0]);
% Control cost matrix
R = 15.0;
% Cross cost matrix
N = [0.0;
     0.0];

%% Compute LQR parameters
[K,S,E] = lqr(A, B, Q, R, N);

%% Nonlinear system equation
f = @(x, u) [x(2);
             -g/l*sin(x(1))-b/(m*l^2)*x(2)+u/(m*l^2)];

%% Create closed-loop polynomial approximation
f2x1 = -g/l*cos(xG(1))-K(1)/(m*l^2);
f2x2 = -b/(m*l^2)-K(2)/(m*l^2);
f2x1_2 = g/l*sin(xG(1));
f2x1_3 = g/l*cos(xG(1));
fcl = @(xbar) [xbar(2)               ;
               xbar(1)*f2x1+xbar(2)*f2x2 + ...
                   xbar(1)^2*f2x1_2/2 + ...
                   xbar(1)^3*f2x1_3/6];

%% Prepare for identifying points
% Initialize vectors to store results
pts_nl = [];
pts_poly = [];

%% Loop through possible points for rho
for rho=0:0.5:20.0
    % Identify points on edge of region of attraction
    [x1s, x2s] = roa_points(S, rho, xG);
    
    % Initialize storage vectors
    Jdots_nl = [0.0];
    Jdots_poly = [0.0];
    
    % Iterate through points on ellipse
    for i=1:length(x1s)
        % Retrieve point on ellipse
        x = [x1s(i);
             x2s(i)];
        % Compute xbar
        xbar = x-xG;
        
        % Compute and add Jdots
        Jdots_nl = [Jdots_nl 2*xbar'*S*f(xG+xbar, uG-K*xbar)];
        Jdots_poly = [Jdots_poly 2*xbar'*S*fcl(xbar)];
    end
    
    % Add maxium Jdot to storage vector
    pts_nl = [pts_nl max(Jdots_nl)];
    pts_poly = [pts_poly max(Jdots_poly)];
end

%% Plot results
figure(1);
plot(0:0.5:20.0, pts_nl, 0:0.5:20.0, pts_poly);
legend('Nonlinear', 'Polynomial');
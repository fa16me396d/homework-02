function [ rho ] = find_rho( xG, S, K, Nm, err )
    % Create symbolic xbar elements
    pvar xbar1 xbar2;
    xbar = [xbar1; xbar2];
    
    % Initialize binary search
    rho_lo = 0.0;
    rho_hi = 100.0;
    
    % Define system polynomials
    J = xbar'*S*xbar;
    Jdot = 2*xbar'*S*fcl(xbar, xG, K);
    hm = monomials(xbar, [0:Nm]);
    h = hm'*mpvar('d', [length(hm), 1]);
    
    % Binary line search to find rho
    while (rho_hi-rho_lo)>0.001
        % Compute midpoint
        rho_mid = mean([rho_hi, rho_lo]);

        % Create proble constraints
        pconstr = [Jdot+h*(rho_mid-J) <= -err*xbar'*xbar;
                   h >= 0];

        % Obtain SOSOpt options
        opts = sosoptions;

        % Try to solve SOS problem
        [info, dopt, sossol] = sosopt(pconstr, xbar, opts);

        % Update rho bounds based on problem feasibility
        if info.feas
            rho_lo = rho_mid;
        else
            rho_hi = rho_mid;
        end
    end
    
    rho = rho_lo;
end
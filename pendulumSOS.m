      checkDependency('spotless');
      checkDependency('mosek');
      
      % Define a new symbolic variable
      xB=msspoly('xb',2);
%       xB = [xB1; xB2];
      
      % Define constants
      g = 9.8;
      m = 1.0;
      l = 0.5;
      b = 0.1;
      
      % Define goal points
      xG = [deg2rad(180.0); 0.0];
      uG = 0.0;
      
      % Define LQR parameters
      S = [174.141056428196, 37.0033554024501; 37.0033554024501, 8.01901074454838];
      K = [9.86756144065337, 2.13840286521290];
      uB = -K*xB;
      
      % Create closed-loop polynomial approximation
      f2x1 = -g/l*cos(xG(1))-K(1)/(m*l^2);
      f2x2 = -b/(m*l^2)-K(2)/(m*l^2);
      f2x1_2 = g/l*sin(xG(1));
      f2x1_3 = g/l*cos(xG(1));
      fcl = [xB(2);
             xB(1)*f2x1+xB(2)*f2x2 + ...
             xB(1)^2*f2x1_2/2 + ...
             xB(1)^3*f2x1_3/6];
      
      % Define the dynamics and Lyapunov function
      x = xB+xG;
      u = uB+uG;
      V = transpose(xB)*S*xB;
      Vdot = diff(V,xB)*fcl;
      
      % construct a SOS program
      prog = spotsosprog;
      prog = prog.withIndeterminate(xB);
      
      % construct the langrange multiplier
      mon=monomials(xB,0:2);
      [prog,c] = prog.newFree(length(mon));
      lambda = c'*mon;
      prog = prog.withSOS(lambda);
      % note: this could all be done with a single line:
      % [prog,lambda] = prog.newSOSPoly(monomials(x,0:4));
      
      % construct the slack variables (which must be positive)
      [prog,slack] = prog.newPos(1);
      
      % add the SOS constraints
      prog = prog.withSOS(-(Vdot+lambda*(10.25-V))-slack*transpose(xB)*xB);
      
      % solve the problem using -slack as the objective
      options = spot_sdp_default_options();
      options.verbose = 0;
      sol = prog.minimize(0,@spot_mosek,options);
      
      % display the results
      slack = double(sol.eval(slack))
      c = double(sol.eval(c))      
      V = sol.eval(V);
      Vdot = sol.eval(Vdot);
      
      [Theta,ThetaDot] = meshgrid(-pi:0.01:pi, -10.0:0.025:10.0);
      Vmesh = dmsubs(V,xB,[Theta(:)';ThetaDot(:)']);
      % surf(Theta,ThetaDot,reshape(Vmesh,size(Theta)));
      contour(Theta+xG(1), ThetaDot+xG(2), reshape(Vmesh, size(Theta)), [10.25,10.25]);
      title('$$ V $$','interpreter','latex','fontsize',15) 
      xlabel('$$ \theta $$','interpreter','latex','fontsize',10)
      ylabel('$$ \dot{\theta} $$','interpreter','latex','fontsize',10)
      
%       % Everything after this is just plotting...
%       
%       xs=-1.2:.05:1.2;
%       clear lambda vl;
%       for i=1:length(xs)
%         lambda(i)=double(subs(c'*mon,x,xs(i)));
%         vl(i)=double(subs(-2*x^2+2*x^4+(c'*mon)*(1-x^2),x,xs(i)));
%       end
%       
%       clf;
%       hold on;
%       plot(xs,-xs+xs.^3,'k','linewidth',2);
%       legend('xdot');
%       axis tight
% %      pause;
%       
%       plot(xs,-2*xs.^2+2*xs.^4,'r','linewidth',2);
%       legend('xdot','Vdot');
% %      pause;
%       
%       plot(xs,lambda,'g','linewidth',2);
%       legend('xdot','Vdot','lambda');
% %      pause;
%       
%       plot(xs,vl,'b','linewidth',2);
%       legend('xdot','Vdot','lambda','Vdot+lambda*(1-V)');